#ifndef _WEIGHTEDDIGRAPH_HPP
#define _WEIGHTEDDIGRAPH_HPP

#include <unordered_map>
#include <unordered_set>
#include <list>
#include <iostream>


// To add our own functions []const operator:
template <typename K, typename V, typename ...ARGS>
class umap : public std::unordered_map<K,V,ARGS...> {
public:
  const V & operator[](const K & rhs) const {
    return this->find(rhs)->second;
  }

  V & operator[](const K & rhs) {
    return std::unordered_map<K,V>::operator[](rhs);
  }

  bool contains(const K & rhs) const {
    return this->find(rhs) != this->end();
  }
};

template <typename K, typename ...ARGS>
class uset : public std::unordered_set<K,ARGS...> {
public:
  bool contains(const K & rhs) const {
    return this->find(rhs) != this->end();
  }
};

template <typename K, typename EDGE_V, typename ...ARGS>
class WeightedDiGraph {
private:
  uset<K, ARGS...> _nodes;
  umap<K, umap<K, EDGE_V, ARGS...>, ARGS...> _node_to_pred_to_weights;
  umap<K, umap<K, EDGE_V, ARGS...>, ARGS...> _node_to_succ_to_weights;
  umap<K, EDGE_V> _empty;
  long _number_of_edges;

public:
  WeightedDiGraph(): _number_of_edges(0) {}
  
  void add_edge(const K & lhs, const K & rhs, EDGE_V w) {
    _nodes.insert(lhs);
    _nodes.insert(rhs);
    _node_to_succ_to_weights[lhs][rhs] = w;
    _node_to_pred_to_weights[rhs][lhs] = w;
    ++_number_of_edges;
  }

  void remove_edge(const K & lhs, const K & rhs) {
    _node_to_succ_to_weights[lhs].erase(rhs);
    _node_to_pred_to_weights[rhs].erase(lhs);
    --_number_of_edges;
  }

  void change_weight(const K & lhs, const K & rhs, EDGE_V w){
    _node_to_succ_to_weights[lhs][rhs] = w;
    _node_to_pred_to_weights[rhs][lhs] = w;
  }

  void add_weight(const K & lhs, const K & rhs, EDGE_V w){
    _node_to_succ_to_weights[lhs][rhs] += w;
    _node_to_pred_to_weights[rhs][lhs] += w;
  }

  void clear_graph(){
    _nodes.clear();
    _node_to_pred_to_weights.clear();
    _node_to_succ_to_weights.clear();
    _number_of_edges = 0;
  }

  typename uset<K>::const_iterator begin() const {
    return _nodes.begin();
  }

  typename uset<K>::const_iterator end() const {
    return _nodes.end();
  }

  long number_of_edges() const {
    return _number_of_edges;
  }

  std::size_t number_of_nodes() const {
    return _nodes.size();
  }

  bool contains(const K & rhs) const {
    return _nodes.find(rhs) != _nodes.end();
  }

  const umap<K, EDGE_V> & pred(const K & key) const {
    auto iter = _node_to_pred_to_weights.find(key);
    if (iter == _node_to_pred_to_weights.end())
      return _empty;

    const umap<K, EDGE_V> & predec = iter->second;
    return predec;
  }

  const umap<K, EDGE_V> & operator[](const K & key) const {
    auto iter = _node_to_succ_to_weights.find(key);
    if (iter == _node_to_succ_to_weights.end())
      return _empty;

    const umap<K, EDGE_V> & succ = iter->second;
    return succ;
  }

  // Function should be a lambda (or a functor if necessary)
  template <typename FUNCTION>
  void directed_bfs(std::list<K> queued_nodes, FUNCTION function) const {
    uset<K> visited_nodes;

    while (queued_nodes.size() > 0) {
      K node = queued_nodes.front();
      queued_nodes.pop_front();
      if (visited_nodes.contains(node))
	continue;
    
      visited_nodes.insert(node);
      function(node);

      // Queue up successors:
      for (const std::pair<K,EDGE_V> & node_and_weight : (*this)[node])
	if (! visited_nodes.contains(node_and_weight.first) )
	  queued_nodes.push_front(node_and_weight.first);
    }
  }

  umap<K,long> st_cut(K s_node, int graph_num) const {
    umap<K,long> result;
    for (const K & node : (*this))
      result[node] = -1;


    K node = s_node;
    directed_bfs( {node}, [&result, graph_num](const K & node) {
	result[node] = graph_num;
      });


    return result;
  }


    // Function should be a lambda (or a functor if necessary)
  template <typename FUNCTION>
  void bfs(std::list<K> queued_nodes, FUNCTION function) const {
    uset<K> visited_nodes;

    while (queued_nodes.size() > 0) {
      K node = queued_nodes.front();
      queued_nodes.pop_front();
      if (visited_nodes.contains(node))
	continue;
    
      visited_nodes.insert(node);
      function(node);

      // Queue up predecessors and successors:
      for (const std::pair<K,EDGE_V> & node_and_weight : (*this)[node])
	if (! visited_nodes.contains(node_and_weight.first) )
	  queued_nodes.push_front(node_and_weight.first);
      for (const std::pair<K,EDGE_V> & node_and_weight : pred(node))
	if (! visited_nodes.contains(node_and_weight.first) )
	  queued_nodes.push_front(node_and_weight.first);
    }
  }

  umap<K,long> connected_graphs() const {
    umap<K,long> result;
    for (const K & node : (*this))
      result[node] = -1;

    long graph_num=0;
    for (const K & node : (*this)) {
      if (result[node] == -1) {
	bfs( {node}, [&result, graph_num](const K & node) {
	    result[node] = graph_num;
	  });
	++graph_num;
      }
    }

    return result;
  }

  
};

template <typename K, typename EDGE_V>
std::ostream & operator <<(std::ostream & os, const WeightedDiGraph<K, EDGE_V> & wdg) {
  for (const K & node : wdg) {
    os << node << ":" << std::endl;
    for (const std::pair<K, EDGE_V> & node_and_weight : wdg[node])
      os << '\t' << node << " --> "  << node_and_weight.first << " " << node_and_weight.second << std::endl;
    os << std::endl;
  }
  return os;
}

#endif
