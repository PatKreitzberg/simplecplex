## Simple CPLEX Interface
**Can do normal linear programming, or a special format to solve max-flow/min-cut problems**

## Setup

CPLEX must be installed (of course). The Makefile links to the CPLEX
library so if they are not in default location then the Makefile must
be changed.

The interpreter uses both FLEX and BISON packages so they must be
installed. This is typically as easy as apt-get install flex, apt-get
install bison (if on Linux).

Then, in the main directory enter "make" to compile.

## Running a script

There are two ways of running the program. You may first run
./interpreter.out and then enter the problem line by line. However, if
you mess up you will have to restart. The preferred method would be to
have the problem in some text file then run ./interpreter.out <  <textfile\>

For the two problems included in this git repo that would look like:

    ./interpreter.out < maxflow_problem.txt
    ./interpreter.out < linear_prog_ex.txt

(I am unsure if < works in windows or not, in Linux it just treats the
content of the file as commands in the terminal)

## Max-flow/min-cut

First, you must tell the interpreter that you want to run a
max-flow/min-cut problem by entering the keyword "maxflow" on the
first line.

Then, edges are included in the format
<tail node\> <head node\> <edge weight\>

To tell the solver to solve the problem, use the keyword "solve". Thus
a full problem

        a---10--b         Where edges go s -> a
       /|\       \                       s -> c
      8 | \       4                      a -> b
     /  |  \       \                     b -> t
    s   5   \       t                    a -> c
     \  |    6     /                     a -> d
      4 |     \   7                      c -> d
       \|      \ /                       a -> d
        c---15--d                        d -> t

Would be solved by entering:

    maxflow
    source src
    sink snk
    src a 8
    src c 4
    a b 10
    b snk 4
    a c 5
    a d 6
    c d 15
    a d 6
    d snk 7
    solve     

As written, the **source** is by default 's' and the **sink** is by
default 't', but they may be defined using "source <sourcename>" and
"sink <sinkname>".  There are no safety measures to check that the
source has no incoming edges and the sink has no outgoing edges.

**Getting the min-cut from the max-flow**

The solution presented will show which edges have their capacity met
in the max-flow solution. To find the min-cut of the graph, remove the
edges which have their flow capacity at max and remove them from the
graph. Then, do a search from the sink node to find all nodes
available.  The set of edges which have one node available to the sink
and one not available, form the set of edges in the min-cut.

## Solving a standard linear programming problem

On the first line start with either "min" or "max" and then the
objective function.  The variables are read in in the form
<coef\><varname\> (e.g. 2.5x), NOT <coef\>\*<varname\> (e.g. 2.5*x).
Variable names must start with a letter but may end with a number. x2
is a good variable name whereas 2x reads in a coefficient of 2 for
variable x.

The constraints are added so that the variables are all to the left of
the inequality and the right side is just a scalar. For an upper bound
you may use either '<' or '<='. Eitherway the constraint is put in as
LHS <= RHS, but it is just easier to use '<' instead of '<='. Similar
for lower bounds.

Then, finish with 'solve' keyword and a newline.

A valid LP looks like:

    max 3x1 + 2.8x2
    x1 > 0
    x2 > 0
    x1 + x2 <  9.3
    3x1 + x2 < 18.98
    x1 < 7.32
    x2 < 6.67
    solve

Should give output:

    =======SOLUTION=======
    Solved with status: Optimal solution found
    Objective value: 27.008
    Variable, value: 
    	x2 = 4.46
    	x1 = 4.84


## Solution status

The solution status returns same as calling CPXgetstat() on the CPLEX
object. A more detailed account can be found at https://www.rpi.edu/dept/math/math-programming/cplex66/sun4x_56/doc/refman/html/appendixB.html#151095

Quickly,

    0: No solution available
    1: Found optimal solution
    2: Problem is infeasible
    3: Problem is unbounded
    .
    .
    .

There are codes up to 19 for basic linear programs.

