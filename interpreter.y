/***
 Purpose: Assign expression values to variables.
 Expressions can either be addition, subtraction,
 multiplication, or division between integers or
 previously assigned variables. The expressions
 should be in a hierarchy that interprets the 
 order of operations correctly. Be able to return
 the stored value of an assigned variable by calling
 the name as a single line command.
**/
%{
#include <ilcplex/cplex.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <map>
#include <cstring>
#include <set>  
#include <vector>
#include "CplexInterface.hpp"

extern "C" FILE *yyin;
extern "C" int yylex();
extern "C" int yyparse();

void yyerror(const char *s);
std::pair<char*, double> parse_coef_and_var(char* coef_and_var);
CplexInterface* cplex; // note: has to be a pointer; otherwise it was calling destructor prematurely (no idea why)
bool graph_cut_or_lp;
%}

/*** union of all possible data return types from grammar ***/
%union {
  double fVal;
  char* sVal;
  std::vector<std::pair<char*, double> >* variablePtr;
  std::pair<char*, double>* varAndDigit;
}


/*** Define token identifiers for flex regex matches ***/
%token NUM
%token VARNAME
%token COEF_AND_VAR
%token ADD SUB MULT DIV
%token LEQ
%token GEQ
%token SOLVE
%token PRINT
%token EOL
%token MIN MAX
%token MAXFLOW LP
%token SOURCE SINK

/*** Define return type for grammar rules ***/
%type<sVal> expression
%type<variablePtr> additive_expression
%type<varAndDigit> variable
%type<sVal> COEF_AND_VAR
%type<fVal> NUM
%type<sVal> VARNAME
%%

start: /* NULL */ 
       | start expression EOL {}
       | start additive_expression EOL {}
       | start MIN additive_expression EOL {
           cplex->set_objective_sense(false);
           cplex->set_objective_function($3);
         } 
       | start MAX additive_expression EOL {
           cplex->set_objective_sense(true);
           cplex->set_objective_function($3);
         } 
       | start MAXFLOW EOL {
           graph_cut_or_lp = 0;
           cplex->set_objective_sense(true);
         } 
       | start LP EOL {
           graph_cut_or_lp = 1;
         } 
       | start SOURCE variable EOL {
           cplex->set_source($3->first);
         }
       | start SINK variable EOL {
           cplex->set_sink($3->first);
         }
       | EOL { exit(0); }
       ;

/* MAX FLOW */
expression: VARNAME VARNAME NUM {
  double weight = (double) $3;
  cplex->add_edge($1, $2, weight);
}

/* START EXPRESSION */
expression: SOLVE {
  if (graph_cut_or_lp == 0)
    cplex->solve_maxflow();
  else
    cplex->solve_lp();
}

expression: PRINT {
  cplex->print_obj_sense();
  cplex->print_obj_fun();
  cplex->print_constraints();
  if (graph_cut_or_lp == 0){
    std::cout << "graph cut or lp == 0" << std::endl;
    cplex->print_edges();

  }
}
/* END EXPRESSION */

/* START VARIABLE */
variable: VARNAME {
  std::pair<char*, double> pair = std::make_pair($1, 1.0);
  $$ = &pair;
}

variable: COEF_AND_VAR {
  std::pair<char*, double> pair = parse_coef_and_var($1);
  $$ = &pair;
}
/* END VARIABLE */

/* START ADDITIVE EXPRESSION */
additive_expression:  variable {
  $$ = new std::vector<std::pair<char*, double> >;
  $$->push_back(*$1);
}

additive_expression: additive_expression ADD variable {
  $$->push_back(*$3);
}

additive_expression:  additive_expression LEQ NUM {
  cplex->add_constraint($1, $3, 'L');
}

additive_expression:  additive_expression GEQ NUM {
  cplex->add_constraint($1, $3, 'G');
}

/* END ADDITIVE EXPRESSION */

%%

std::pair<char*, double> parse_coef_and_var(char* coef_and_var){
  std::string coef_and_var_str(coef_and_var);
  int i=0;
  while (isdigit(coef_and_var_str[i]))
    ++i;

  if (coef_and_var_str[i] == '.'){
    ++i;
    while (isdigit(coef_and_var_str[i]))
      ++i;
  }

  std::string digit = coef_and_var_str.substr(0, i);
  std::string var   = coef_and_var_str.substr(i, coef_and_var_str.length());
  double val = atof(digit.c_str());
  char* var_cstr = new char[var.length()+1];

  strcpy(var_cstr, var.c_str());
  std::pair<char*, double> pair = std::make_pair(var_cstr, val);
  return pair;
}

int main(int argc, char **argv) {
  std::cout << "**For instructions on use check README.md**" << std::endl << std::endl << std::endl;
  graph_cut_or_lp = true;
  cplex = new CplexInterface();

  FILE *fh;
  if (argc == 2 && (fh = fopen(argv[1], "r")))
    yyin = fh;


  yyparse();


  delete cplex;
}

/* Display error messages */
void yyerror(const char *s) {
  printf("ERROR: %s\n", s);
}
