SYSTEM     = x86-64_linux
LIBFORMAT  = static_pic

CPLEXDIR      = /opt/ibm/ILOG/CPLEX_Studio128/concert/
CONCERTDIR    = /opt/ibm/ILOG/CPLEX_Studio128/cplex/

# ---------------------------------------------------------------------
# Compiler options 
# ---------------------------------------------------------------------

CCOPT = -O3 -mtune=native -fopenmp -march=native -DIL_STD # Must be included for CPLEX or else it throws exception "<iostream.h> not found"

# ---------------------------------------------------------------------
# Link options and libraries
# ---------------------------------------------------------------------

CPLEXLIBDIR   = $(CPLEXDIR)/lib/$(SYSTEM)/$(LIBFORMAT)
CONCERTLIBDIR = $(CONCERTDIR)/lib/$(SYSTEM)/$(LIBFORMAT)

CCLNDIRS  = -L$(CPLEXLIBDIR) -L$(CONCERTLIBDIR)
CCLNFLAGS = -lconcert -lilocplex -lcplex -lm -lpthread -ldl


CONCERTINCDIR = $(CONCERTDIR)/include
CPLEXINCDIR   = $(CPLEXDIR)/include

CCFLAGS = $(CCOPT) -I$(CPLEXINCDIR) -I$(CONCERTINCDIR)
CCFLAGSDEBUG = -mtune=native -march=native -DIL_STD -I$(CPLEXINCDIR) -I$(CONCERTINCDIR) 

CC=g++

CPLEXFLAGS = -DIL_STD -I$(CPLEXINCDIR) -I$(CONCERTINCDIR) $(CCLNDIRS)

all: interpreter.out

interpreter.tab.c: interpreter.y
	bison -d interpreter.y

lex.yy.c: interpreter.l
	flex interpreter.l

interpreter.out: interpreter.l interpreter.y interpreter.tab.c lex.yy.c CplexInterface.hpp
	 $(CC) -g $(CFLAGS) $(CPLEXFLAGS) -std=c++17 -Wall interpreter.tab.c lex.yy.c -lfl -o interpreter.out $(CCLNFLAGS)
