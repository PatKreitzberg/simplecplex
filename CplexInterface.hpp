#ifndef __CPLEXINTERFACE__HPP
#define __CPLEXINTERFACE__HPP

#include <unordered_map>
#include "WeightedDiGraph.hpp"

class CplexInterface{
private:
  
  void print_sol_status(int status){
    switch(status) {
    case 0:
      std::cout << "No solution found";
      break;
    case 1:
      std::cout << "Optimal solution found";
      break;
    case 2:
      std::cout << "Problem is infeasible";
      break;
    case 3:
      std::cout << "Problem is unbounded";
      break;
    default:
      std::cout << "Look up CPLEX solution status " << status;
    }
  }
  
  /* CONSTRAINT STRUCT DECLARATION */
  struct constraint{
    constraint(std::vector<std::pair<char*, double> >* variable_and_coef_ptr_param, double bound_param, char sense_param):
      variable_and_coef_ptr(variable_and_coef_ptr_param),
      bound(bound_param),
      sense(sense_param)
    {}
    
    std::vector<std::pair<char*, double> >* variable_and_coef_ptr;
    double bound;
    char sense;
    
    int size(){
      return variable_and_coef_ptr->size();
    }
    
  };
  /* END OF STRUCT DECLARATION */
  
  CPXENVptr _env; 
  CPXLPptr  _lp;
  WeightedDiGraph<std::string, std::pair<double, long> > _wdg;
  int _status;
  int _number_columns;
  int _number_of_constraints;
  long _edge_column_index;
  std::unordered_map<std::string, int> _variable_to_column;
  std::vector<constraint*> _vector_of_constraints;
  std::vector<std::pair<char*, double> >* _objective_function;
  char _objective_function_sense;
  bool _set_obj_fun, _nonzero_constraints, _set_obj_sense;
  char* _source;
  char* _sink;
  
  __attribute__(( always_inline ))
  bool _variable_initialized(char* var) const {
    if (_variable_to_column.find(var) == _variable_to_column.end())
      return false;
    return true;
  }
  
  __attribute__(( always_inline ))
  void _add_variable_if_necessary(char* var){
    if (!_variable_initialized(var)){
      _variable_to_column.insert(std::make_pair(var, _number_columns));
      ++_number_columns;
    }
  }
  
  __attribute__(( always_inline ))
  int column_index_for_variable(char* var){
    return _variable_to_column[std::string(var)];
  }
  
  void _add_constraints(){
    const int number_of_constraints = _vector_of_constraints.size();
    int number_nonzero_coeffs = 0;
    for (auto &con : _vector_of_constraints)
      for (auto &var_and_value : (*con->variable_and_coef_ptr)){
        _add_variable_if_necessary(var_and_value.first);
        ++number_nonzero_coeffs;
      }
    
    // rmatbeg
    int* beginning_index_for_constraint_in_matrix = (int *) malloc (number_of_constraints * sizeof (int));
    // rmatind
    int* variable_matrix_index = (int *) malloc (number_nonzero_coeffs * sizeof (int)); 
    // rmatval
    double* variable_matrix_value = (double *) malloc (number_nonzero_coeffs * sizeof (double));
    
    double* rhs = (double *) malloc (number_of_constraints * sizeof (double));
    char* sense = (char *)   malloc (number_of_constraints * sizeof (double));
    
    int constraint_count = 0;
    int index_in_matrix = 0;
    
    for (auto &con : _vector_of_constraints){
      const double constraint_rhs = con->bound;
      const char constraint_sense = con->sense;
      const std::vector<std::pair<char*, double> >* variable_and_coef_ptr = con->variable_and_coef_ptr;
      beginning_index_for_constraint_in_matrix[constraint_count] = index_in_matrix;
      
      for (auto &var_and_value : (*variable_and_coef_ptr)){
        char* var = var_and_value.first;
        const int column_index = column_index_for_variable(var);
        const double value = var_and_value.second;
        variable_matrix_index[index_in_matrix] = column_index;
        variable_matrix_value[index_in_matrix] = value;
        ++index_in_matrix;
      }
      sense[constraint_count] = constraint_sense;
      rhs[constraint_count] = constraint_rhs;
      ++constraint_count;      
    }
    
    CPXaddrows(_env,                        // CPLEX environment
               _lp,                         // CPLEX linear program
               _variable_to_column.size(),  // Number of new columns being added
               constraint_count,            // Number of new rows being added
               number_nonzero_coeffs,       // Number nonzero constraint coeffs added
               rhs,                         // Right hand side of constraint (upper or lower bound)
               sense,                       // Sense of constraint (less than, greater than, or equal)
               beginning_index_for_constraint_in_matrix, // start of constraint in the arrays
               variable_matrix_index,                    // column index for variable
               variable_matrix_value,                    // value for variable
               NULL,                                     // column name
               NULL                                      // row name
               );
    free(beginning_index_for_constraint_in_matrix);
    free(variable_matrix_index);
    free(variable_matrix_value);
    free(rhs);
    free(sense);
  }
  
  void _set_objective_function(){
    int current_index = 0;
    int number_coefs_changed = _objective_function->size();
    int* changed_coefs_index = (int*) malloc(number_coefs_changed * sizeof(int));
    double* new_coefs = (double*) malloc(number_coefs_changed * sizeof(double));
    
    
    for (auto &var_and_val : (*_objective_function)){
      changed_coefs_index[current_index] = column_index_for_variable(var_and_val.first);
      new_coefs[current_index] = var_and_val.second;
      ++current_index;
    }    
    
    CPXchgobj(_env, _lp, number_coefs_changed, changed_coefs_index, new_coefs);
    free(changed_coefs_index);
    free(new_coefs);
  }
  
  void _solve_lp(){
    _add_constraints();
    _set_objective_function();
    
    CPXlpopt (_env, _lp);
    int cur_numrows = CPXgetnumrows(_env, _lp);
    int cur_numcols = CPXgetnumcols(_env, _lp);
    double* x     = (double *) malloc (cur_numcols * sizeof(double));
    double* slack = (double *) malloc (cur_numrows * sizeof(double));
    double* dj    = (double *) malloc (cur_numcols * sizeof(double));
    double* pi    = (double *) malloc (cur_numrows * sizeof(double));
    int      solstat;
    double   objval;
    
    CPXsolution (_env, _lp, &solstat, &objval, x, pi, slack, dj);
    std::cout << "=======SOLUTION=======" << std::endl;
    std::cout << "Solved with status: ";
    print_sol_status(solstat);
    std::cout << std::endl;
    std::cout << "Objective value: " << objval << std::endl;
    std::cout << "Variable, value: "<< std::endl;
    for (auto &it : _variable_to_column)
      std::cout << "\t" << it.first << " = " <<  x[it.second] << std::endl;
    
    free(x);
    free(slack);
    free(dj);
    free(pi);
  }
  
public:
  CplexInterface():
    _number_columns(0),
    _number_of_constraints(0),
    _edge_column_index(0),
    _set_obj_fun(false),
    _nonzero_constraints(false),
    _set_obj_sense(false)
  {
    _source = new char[1];
    _source[0] = 's';
    _sink = new char[1];
    _sink[0] = 't';
    _env = CPXopenCPLEX(&_status);
    _lp = CPXcreateprob(_env, &_status, "lpex1");
  }
  
  ~CplexInterface(){
    for (unsigned i = 0; i < _vector_of_constraints.size(); ++i)
      delete _vector_of_constraints[i];
    
    /* Free up the problem as allocated by CPXcreateprob, if necessary */
    int status;
    if ( _lp != NULL ) {
      status = CPXfreeprob (_env, &_lp);
      if ( status ) {
        fprintf (stderr, "CPXfreeprob failed, error code %d.\n", status);
      }
    }
    return;
    
    /* Free up the CPLEX environment, if necessary */
    if ( _env != NULL ) {
      status = CPXcloseCPLEX (&_env);
      
      /* Note that CPXcloseCPLEX produces no output,
         so the only way to see the cause of the error is to use
         CPXgeterrorstring.  For other CPLEX routines, the errors will
         be seen if the CPXPARAM_ScreenOutput indicator is set to CPX_ON. */
      
      if ( status ) {
        char  errmsg[CPXMESSAGEBUFSIZE];
        fprintf (stderr, "Could not close CPLEX environment.\n");
        CPXgeterrorstring (_env, status, errmsg);
        fprintf (stderr, "%s", errmsg);
      }
    }
  }
  
  void print_edges(){
    std::cout << "print edges" << std::endl;
    auto iter = _wdg.begin();
    while (iter != _wdg.end()) {
      for (auto x : _wdg[*iter]){
        std::cout << *iter << " --> " << x.first << " " << x.second.first << std::endl;
      }
      ++iter;
    }
  }
  
  void set_source(char* source){
    _source = source;
  }
  
  void set_sink(char* sink){
    _sink = sink;
  }
  
  void add_edge(char* a, char* b, double weight){
    std::string str_a(a);
    std::string str_b(b);
    std::pair<double, long> weight_and_column_index = std::make_pair(weight, _edge_column_index);
    ++_edge_column_index;
    _wdg.add_edge(str_a, str_b, weight_and_column_index);
  }
  
  void set_objective_sense(char min_or_max){
    _set_obj_sense = true;    
    if (!min_or_max)
      CPXchgobjsen(_env, _lp, CPX_MIN);      
    else
      CPXchgobjsen(_env, _lp, CPX_MAX);
  }
  
  void add_constraint(std::vector<std::pair<char*, double> >* variable_and_coef_ptr, double bound, char sense){
    _nonzero_constraints = true;    
    _vector_of_constraints.push_back(new constraint(variable_and_coef_ptr, bound, sense));
    _number_of_constraints += 1;
  }
  
  void set_objective_function(std::vector<std::pair<char*, double> >* variable_and_coef_ptr){
    _set_obj_fun = true;
    _objective_function = variable_and_coef_ptr;
  }
  
  void print_constraints(){
    if (!_nonzero_constraints)
      return;
    
    std::cout << "Constraints" << std::endl;
    for (auto &con : _vector_of_constraints){
      for (auto &var_and_coef : (*con->variable_and_coef_ptr))
        std::cout << var_and_coef.second << " * " << var_and_coef.first <<  " + ";
      
      if (con->sense == 'L')
        std::cout << "<=";
      else if (con->sense == 'G')
        std::cout << ">=";
      std::cout << con->bound << std::endl;
    }      
  }
  
  void print_obj_fun(){
    if (!_set_obj_fun)
      return;
    std::cout << "Objective function ";
    for (auto &var_and_coef : (*_objective_function))
      std::cout << var_and_coef.second <<  " * " << var_and_coef.first << " + ";
    std::cout << std::endl;
  }
  
  void print_obj_sense(){
    std::cout << "obj " << CPXgetobjsen(_env, _lp) << std::endl;
    if (!_set_obj_sense)
      return;
    
    std::cout << "Objective sense: ";
    if (CPXgetobjsen(_env, _lp) == 1)
      std::cout << "Minimize" << std::endl;
    if (CPXgetobjsen(_env, _lp) == -1)
      std::cout << "Maximize" << std::endl;
    else
      std::cout << "UNSET" << std::endl;
  }
  
  void _set_obj_function_for_maxflow(){
    // objective function is to maximize flow    
    const int number_of_variables = _wdg.number_of_edges();
    double* obj_fun = (double*) malloc( number_of_variables * sizeof(double) );
    double* variable_lower_bounds = (double*) malloc( number_of_variables * sizeof(double) );
    double* variable_upper_bounds = (double*) malloc( number_of_variables * sizeof(double) );

    for (auto &node : _wdg){
      for (const std::pair<std::string, std::pair<double, long> > & b : _wdg[node]) {
        const double weight = b.second.first;
        const long index = b.second.second;
        if (node.compare(_source) == 0)
          obj_fun[index] = 1.0;
        else
          obj_fun[index] = 0.0;
        variable_lower_bounds[index] = 0.0;
        variable_upper_bounds[index] = weight;
      }
    }

    //for (auto &node : _wdg)
    //  for (const std::pair<std::string, std::pair<double, long> > & b : _wdg[node]) {
    //    std::cout << node << " --> " << b.first << " index " << b.second.second << " obj fun " << obj_fun[b.second.second] << std::endl;
    //  }
    
    CPXnewcols(_env,                   // CPLEX environment
               _lp,                    // CPLEX linear program
               number_of_variables,    // Number of columns being added
               obj_fun,                // Coeffecients in objective function
               variable_lower_bounds,  // Lower bounds for the variables
               variable_upper_bounds,  // Upper bounds for the variables
               NULL,                   // Contains type of variables
               NULL                    // Column names
               );

    free(obj_fun);
    free(variable_lower_bounds);
    free(variable_upper_bounds);    
  }
  
  void _add_edge_flow_out_of_nodes(const std::string &node, int* variable_matrix_index, double* variable_matrix_value, int &number_nonzero_coeffs, int &index_in_matrix) {
    for (const auto &node_and_weight_and_column : _wdg[node]){
      // edge goes from node to node_and_weight_and_column
      variable_matrix_index[index_in_matrix] = node_and_weight_and_column.second.second;
      variable_matrix_value[index_in_matrix] = 1;
      ++number_nonzero_coeffs;
      ++index_in_matrix;
    }
  }

  void _add_edge_flow_in_to_nodes(const std::string &node, int* variable_matrix_index, double* variable_matrix_value, int &number_nonzero_coeffs, int &index_in_matrix) {
    for (const auto &node_and_weight_and_column : _wdg.pred(node)){
      variable_matrix_index[index_in_matrix] = node_and_weight_and_column.second.second;
      variable_matrix_value[index_in_matrix] = -1;
      ++number_nonzero_coeffs;	
      ++index_in_matrix;
    }
  }

  void _add_edges_to_cplex() {
    const int number_of_constraints = _wdg.number_of_nodes();
    const int max_possible_terms = _wdg.number_of_edges() * _wdg.number_of_nodes();
    int number_nonzero_coeffs = 0;
    
    // rmatbeg
    int* beginning_index_for_constraint_in_matrix = (int *) malloc (number_of_constraints * sizeof (int));
    // rmatind
    int* variable_matrix_index = (int *) malloc (max_possible_terms * sizeof (int)); 
    // rmatval
    double* variable_matrix_value = (double *) malloc (max_possible_terms * sizeof (double));
    
    double* rhs = (double *) malloc (number_of_constraints * sizeof (double));
    char* sense = (char *)   malloc (number_of_constraints * sizeof (double));
    int constraint_count = 0;
    int index_in_matrix = 0;
    
    // Add each edge as a variable
    for (const auto &node : _wdg){
      if (node.compare(_source) == 0 || node.compare(_sink) == 0)
        continue;
      
      beginning_index_for_constraint_in_matrix[constraint_count] = index_in_matrix;
      // Edges flowing out of node
      _add_edge_flow_out_of_nodes(node, variable_matrix_index, variable_matrix_value, number_nonzero_coeffs, index_in_matrix);
      // Edges flowing into node
      _add_edge_flow_in_to_nodes(node, variable_matrix_index, variable_matrix_value, number_nonzero_coeffs, index_in_matrix);

      // rhs =0 so that flow in is flow out
      rhs[constraint_count] = 0;
      sense[constraint_count] = 'E';
      ++constraint_count;
    }

    CPXaddrows(_env, _lp, 0, constraint_count, number_nonzero_coeffs, rhs, sense, beginning_index_for_constraint_in_matrix, variable_matrix_index, variable_matrix_value, NULL, NULL);

    free(beginning_index_for_constraint_in_matrix);
    free(variable_matrix_index);
    free(variable_matrix_value);
    free(rhs);
    free(sense);
  }
  
  void _solve_maxflow(){
    int cur_numrows = CPXgetnumrows(_env, _lp);
    int cur_numcols = CPXgetnumcols(_env, _lp);
    CPXlpopt (_env, _lp);
    
    double* x     = (double *) malloc (cur_numcols * sizeof(double));
    double* slack = (double *) malloc (cur_numrows * sizeof(double));
    double* dj    = (double *) malloc (cur_numcols * sizeof(double));
    double* pi    = (double *) malloc (cur_numrows * sizeof(double));
    int      solstat;
    double   objval;
    CPXsolution (_env, _lp, &solstat, &objval, x, pi, slack, dj);
    
    std::cout << "=======SOLUTION=======" << std::endl;    
    std::cout << "Solved with status: ";
    print_sol_status(solstat);
    std::cout << std::endl;
    std::cout << "Max flow/min-cut :" << objval << std::endl;
    std::cout << "Variable, value: " << std::endl;
    std::cout << "Edges which have max capacity flow (candidates to be in min-cut)"  << std::endl;
    for (const auto &node : _wdg){
      if (node.compare(_sink) == 0)
        continue;
      for (const auto &node_and_weight_and_column : _wdg[node])
        if (x[node_and_weight_and_column.second.second] == node_and_weight_and_column.second.first)
          std::cout << node << " --> " << node_and_weight_and_column.first << " = " << x[node_and_weight_and_column.second.second] << std::endl;
    }
    std::cout << std::endl;
    std::cout << "All edges and their flow:" << std::endl;
    for (const auto &node : _wdg){
      if (node.compare(_sink) == 0)
        continue;
      for (const auto &node_and_weight_and_column : _wdg[node]){
        std::cout << node << " --> " << node_and_weight_and_column.first <<  " = " << x[node_and_weight_and_column.second.second] << std::endl;
        
      }
    }
    free(x);
    free(slack);
    free(dj);
    free(pi);
  }
  
  void solve_maxflow(){
    // objective sense: max
    CPXchgobjsen(_env, _lp, CPX_MAX);
    
    // objective function the flow out of source
    _set_obj_function_for_maxflow();
    
    // add edge constraints
    _add_edges_to_cplex();
    
    // solve and print
    _solve_maxflow();
  }
  
  void solve_lp(){
    if (_set_obj_fun && _nonzero_constraints && _set_obj_sense)
      _solve_lp();
    else{
      if (!_set_obj_fun)
        std::cout << "ERROR: Need to set objective function" << std::endl;
      if (!_nonzero_constraints)
        std::cout << "ERROR: Need to set constraints" << std::endl;
      if (!_set_obj_sense)
        std::cout << "ERROR: Need to set objective sense (min or max)" << std::endl;
    }
  }
};
#endif
